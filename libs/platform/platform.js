import {readFile, readFileSync, saveFile, saveFileSync} from './platform/file';
import {removeStorage, setStorage, getStorage} from './platform/cache';
import {playSound, playMusic, stopSound, stopMusic} from './platform/sound';

function Platform() {
	this.readFile = readFile;
	this.readFileSync = readFileSync;
	this.saveFile = saveFile;
	this.saveFileSync = saveFileSync;

	this.removeStorage = removeStorage;
	this.setStorage = setStorage;
	this.getStorage = getStorage;

	this.playSound = playSound;
	this.playMusic = playMusic;
	this.stopMusic = stopMusic;
	this.stopSound = stopSound;

	this.systemInfo = null;
	this.launchOptions = null;
}

Platform.prototype = {
	init:function() {
		// brand	string	手机品牌	1.5.0
		// model	string	手机型号	
		// pixelRatio	number	设备像素比	
		// screenWidth	number	屏幕宽度	1.1.0
		// screenHeight	number	屏幕高度	1.1.0
		// windowWidth	number	可使用窗口宽度	
		// windowHeight	number	可使用窗口高度	
		// statusBarHeight	number	状态栏的高度	1.9.0
		// language	string	微信设置的语言	
		// version	string	微信版本号	
		// system	string	操作系统版本	
		// platform	string	客户端平台	
		// fontSizeSetting	number	用户字体大小设置。以“我-设置-通用-字体大小”中的设置为准，单位 px。	1.5.0
		// SDKVersion	string	客户端基础库版本	1.1.0
		// benchmarkLevel	number	(仅Android小游戏) 性能等级，-2 或 0：该设备无法运行小游戏，-1：性能未知，>=1 设备性能值，该值越高，设备性能越好 (目前设备最高不到50)
		this.systemInfo = wx.getSystemInfoSync();

		// scene	number	启动小游戏的场景值	
		// query	Object	启动小游戏的 query 参数	
		// shareTicket	string	shareTicket，详见获取更多转发信息	
		// referrerInfo	object	来源信息。从另一个小程序、公众号或 App 进入小程序时返回。否则返回 {}。(参见后文注意)
		this.launchOptions = wx.getLaunchOptionsSync();
	},

	/**
	 * 退出当前小游戏
	 */
	exit:function(complete, success, fail){
		wx.exitMiniProgram({complete, success, fail});
	},

	onShow:function(callback) {
		wx.onShow(callback)
	},

	onHide:function(callback) {
		wx.onShow(callback)
	},

	offShow:function(callback){
		wx.offShow(callback)
	},

	offHide:function(callback) {
		wx.offShow(callback)
	},

	onError:function(callback) {
		wx.onError(callback);
	},

	offError:function(callback) {
		wx.offError(callback);
	},

	// 设置系统剪贴板的内容
	// data	string		是	剪贴板的内容	
	// success	function		否	接口调用成功的回调函数	
	// fail	function		否	接口调用失败的回调函数	
	// complete	function		否	接口调用结束的回调函数（调用成功、失败都会执行）
	setClipboardData:function(object) {
		wx.setClipboardData(object);
	},

	// 获取系统剪贴板的内容
	// success	function		否	接口调用成功的回调函数	
	// fail	function		否	接口调用失败的回调函数	
	// complete	function		否	接口调用结束的回调函数（调用成功、失败都会执行）
	getClipboardData:function(object) {
		wx.getClipboardData(object);
	}
};

let platform = new Platform();
window.platform = platform;
export default platform;
