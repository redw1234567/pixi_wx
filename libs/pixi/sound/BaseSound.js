export class BaseSound {
    constructor() {
        this.src = "";
        this.isPlaying = false;
        this.isPaused = false;
    }

    play (value) {
        this.isPlaying = true;
        this.isPaused = false;

        let type = typeof value;
        if (type == "string") {
            this.src = options;
        }

        return true;
    }

    pause() {
        if (this.isPaused || !this.isPlaying) {
            return false;
        }
        this.isPlaying = false;
        this.isPaused = true;
        return true;
    }

    resume() {
        if (!this.isPaused || this.isPlaying) {
            return false;
        }
        this.isPlaying = true;
        this.isPaused = false;
        return true;
    }

    stop() {
        if (!this.isPaused && !this.isPlaying) {
            return false;
        }
        this.isPlaying = false;
        this.isPaused = false;
        return true;
    }
}