import WXSoundManager from './wx/WXSoundManager';

export default class SoundPlugin {
    static init(options) {
        if (window.wx) {
            this.sound = new WXSoundManager(options)
        }
        console.log("注册sound插件", options)
    }
}