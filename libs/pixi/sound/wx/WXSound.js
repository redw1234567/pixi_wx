import { BaseSound } from './../BaseSound';

export default class WXSound extends BaseSound {
    constructor() {
        super();

        this.soundContext = wx.createInnerAudioContext();
    }

    play(value) {
        super.play(value);

        let src = this.src;
        this.soundContext.src = src;
    }

    pause() {
        let canPause = super.pause();
        if (canPause) {
            this.soundContext.pause();
        }
    }

    stop() {
        let canStop = super.stop();
        if (canStop) {
            this.soundContext.stop();
        }
    }

    resume() {
        let canResume = super.resume();
        if (canResume) {
            this.soundContext.resume();
        }
    }

    isFree() {
        
    }

}