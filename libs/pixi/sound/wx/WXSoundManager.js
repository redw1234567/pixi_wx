import WXSound from './WXSound';

export default class WXSoundManager{
    constructor() {
        this.soundList = [];
        this.curMusic = null;
    }

    // 播放声音
    playSound(src) {
        let sound = null;
        let soundList = this.soundList;

        for (let i = 0, len = soundList.length; i < len; i++) {
            if (soundList[i].isFree()) {
                sound = soundList[i];
                break;
            }
        }
        if (!sound) {
            sound = new WXSound();
            soundList.push(sound);
        }
        sound.play(src);
    }

    // 播放音乐
    playMusic(src) {
        let curMusic = this.curMusic;
        if (!curMusic) {
            curMusic = new WXSound();
            this.curMusic = curMusic;
        }
        if (curMusic) {
            if (curMusic.src != src) {
                curMusic.src = src;
            }
        }
        if (curMusic.paused) {
            curMusic.play();
        }
    }

    // 停止声音
    stopSound(sound) {
        let soundList = this.soundList;
        for (let i = 0; i < soundList.length; i++) {
            let sound = soundList[i];
            sound.stop();
        }
    }

    // 停止音乐(一直循环的)
    stopMusic() {
        let curMusic = this.curMusic;
        if (curMusic) {
            curMusic.stop();
        }
    }
}

