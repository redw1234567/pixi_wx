// 加载小游戏包体资源加载
export default class WXLoadPlugin
{
    /**
     * Called after a resource is loaded.
     * @see PIXI.Loader.loaderMiddleware
     * @param {PIXI.LoaderResource} resource
     * @param {function} next
     */
    static pre(resource, next){
        let baseUrl = this.baseUrl;
        if (!baseUrl || baseUrl.indexOf("http") != 0) {
            let {url, extension} = resource;
            if (extension == "json") {
                let fileManager = wx.getFileSystemManager();
                let exsit = fileManager.accessSync(url);
                if (!exsit) {
                    let content = fileManager.readFileSync(url, "utf8")
                    let json = JSON.parse(content);
                    resource._flags = 2;
                    // resource.isComplete = true;
                    resource.type = PIXI.LoaderResource.TYPE.JSON;
                    resource.data = json;
                    next();
                }
            } else {
                next();
            }
        } else {
            next();
        }
    }
}