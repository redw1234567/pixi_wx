import Clock from './Clock';
export default class ClockPlugin {
    static init(options) {
        options = Object.assign({
            autoStart: true,
            priority:PIXI.UPDATE_PRIORITY.NORMAL
        }, options);

        console.log("注册clock插件", options)
        this.clock = new Clock(options);
        this.ticker.add(this.clock.update, this.clock, options.priority)
    }
}