export default class TimerEvent extends PIXI.utils.EventEmitter {
    constructor(options) {
        super();

        // 延迟多少毫秒执行
        this.delay = 0;
        // 剩余的repeate次数
        this.repeatCount = 0;
        //孤儿待删除的
        this.orphan = false;

        this.elapsed = 0;
        this.paused = false;
        this.hasDispatched = false;

        this.callback = null;
        this.callbackContext = null;
        this.args = null;

        this.setInfo(options);
    }

    setInfo(options) {
        if (!options) {
            console.error("没有设置时间信息~")
            options = {};
        }
        const repeat = options.repeat || 1;
        const loop = options.loop || false;
        const startAt = options.startAt || 0;

        this.delay = options.delay || 0;
        this.paused = options.paused || false;
        this.elapsed = startAt;
        this.hasDispatched = false;
        this.orphan = false;
        this.repeatCount = (repeat === -1 || loop) ? 999999999999 : repeat;

        this.callback = options.callback;
        this.callbackScope = options.callbackContext
        this.args = options.args;

        return this;
    }

    getProgress() {
        return (this.elapsed / this.delay);
    }

    getOverallProgress() {
        if (this.repeat > 0) {
            var totalDuration = this.delay + (this.delay * this.repeat);
            var totalElapsed = this.elapsed + (this.delay * (this.repeat - this.repeatCount));

            return (totalElapsed / totalDuration);
        } else {
            return this.getProgress();
        }
    }

    getRepeatCount() {
        return this.repeatCount;
    }

    getElapsed() {
        return this.elapsed;
    }

    getElapsedSeconds() {
        return this.elapsed * 0.001;
    }

    remove(dispatchCallback) {
        if (dispatchCallback === undefined) { dispatchCallback = false; }
        this.elapsed = this.delay;
        this.hasDispatched = !dispatchCallback;
        this.repeatCount = 0;
    }

    destroy() {
        this.callback = null;
        this.callbackContext = null;
        this.args = null;
    }
}