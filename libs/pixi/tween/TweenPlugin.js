let Easing = require('./Easing').default;
let Tween = require('./Tween').default;
let TweenManager = new require('./TweenManager').default;

export default class TweenPlugin {
    static init(options) {
        options = Object.assign({
            priority:PIXI.UPDATE_PRIORITY.NORMAL
        }, options)
        let tweenManager = new TweenManager(options);

        this.tween = {
            Easing,
            Tween,
            tweenManager
        }
        this.ticker.add(tweenManager.update, tweenManager, options.priority)
        console.log("注册tween插件", options)
    }

    static destory() {

    }
}