import './libs/weapp-adapter.js'
import './libs/pixi/pixi.min.js'
import './libs/pixi/unsafe-eval.min'
import ClockPlugin from './libs/pixi/clock/ClockPlugin'
import TweenPlugin from './libs/pixi/tween/TweenPlugin'
import SoundPlugin from './libs/pixi/sound/SoundPlugin'
import WXLoadPlugin from './libs/pixi/load/WXLoadPlugin';

PIXI.Application.registerPlugin(ClockPlugin)
PIXI.Application.registerPlugin(TweenPlugin)
PIXI.Application.registerPlugin(SoundPlugin)

PIXI.Loader.registerPlugin(WXLoadPlugin)