let childProcess = require('child_process');
let exec = childProcess.exec;

/**
 * 打包文件
 * @param sourceDir
 * @param targetFile
 * @returns {Promise}
 */
function pack(sourceDir, targetFile) {
    return new Promise((resolve, reject)=>{
        exec(`TextureMerger.exe -p ${sourceDir} -o ${targetFile}`,(error, stdout, stderr) => {
            console.log(sourceDir, targetFile);
            if (error) {
                console.log("打包文件:",sourceDir);
                reject();
            } else {
                resolve(`打包${sourceDir}成功`);
            }
        });
    });
}
module.exports = pack;

