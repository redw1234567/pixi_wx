const { spawn } = require('child_process');
function exec(command, param, options) {
    const p = spawn(command, param, options);
    console.log(command, "-----------------")
    p.stdout.on('data', (e) => {
        console.log("out:", command, e.toString());
    });
    p.stderr.on('data', (e) => {
        console.log("error:", command, e.toString());
    });
    p.on('close', (e) => {
        console.log("close:", e);
        // cb();
    });
    return p;
}


module.exports= exec;

