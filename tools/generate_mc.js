const fs = require('fs')
const path = require('path')


module.exports = function(dir) {
    // let dir = './assets/mc'
    const files = fs.readdirSync(dir)
    for (let i = 0; i < files.length; i++) {
        let file = files[i]
        let extname = path.extname(file)
        if (extname == ".json") {
            let str = fs.readFileSync(path.join(dir, file))
            let mc = JSON.parse(str);
            if (mc["mc"]) {
                let resources = mc["res"];
                let name = path.basename(file, ".json")
                let content = mc["mc"][name]
                if (!content) continue;
                let {frames,labels, events} = content

                let sheet = {};
                let map = {};
                let frameObjs = [];
                for (let j = 0; j < frames.length; j++) {
                    let {x, y, res, duration} = frames[j];
                    let rect = resources[res]
                    let item = {
                        frame:{...rect}
                    };
                    if (duration > 1) {
                        item.duration = duration;
                    }
                    map[`${name}${j+1}.png`] = item;
                    frameObjs.push({name:`${name}${j+1}.png`, x, y})
                    while (duration > 1) {
                        duration--;
                        frameObjs.push(null);
                    }
                }
                sheet.frames = map;

                let meta = {
                    app:"node parse egret",
                    version: "1.0",
                    image: `${name}.png`,
                    format: "RGBA8888",
                    scale: 1,
                    smartupdate: "$TexturePacker:SmartUpdate:2f213a6b451f9f5719773418dfe80ae8$"
                };
                sheet.meta = meta;

                if (labels) {
                    let animations = {};
                    for (let j = 0; j < labels.length; j++) {
                        let start = labels[j].frame;
                        let end = labels[j].end;
                        let animalName = labels[j].name;
                        animations[animalName] = [];
                        for (let k = start; k <= end; k++) {
                            if (frameObjs[k - 1]) {
                                animations[animalName].push(frameObjs[k - 1].name)
                            }
                        }
                    }
                    sheet.animations = animations;
                }

                let pos = [];
                let lastFrameObj = null;
                for (let j = 0; j < frameObjs.length; j++) {
                    if (frameObjs[j]) {
                        pos.push(frameObjs[j].x, frameObjs[j].y)
                        lastFrameObj = frameObjs[j];
                    } else {
                        if (lastFrameObj) {
                            pos.push(lastFrameObj.x, lastFrameObj.y)
                        } else {
                            pos.push(0, 0)
                        }
                    }
                }
                sheet.pos = pos;

                if (events && events.length > 0) {
                    sheet.events = events;
                }

                fs.writeFileSync(path.join(dir, file), JSON.stringify(sheet, null, 2));
                
            }
        }
        
    }
}
