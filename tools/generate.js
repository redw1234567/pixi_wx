const config = require('./config')
const generateSheet = require('./generate_sheet')
const generateMC = require('./generate_mc')
// const generateSpine = require('./generate_spine')
const generateSkin = require('./generate_skin')
const generateViewId = require('./generate_viewid')

start();

function start() {
    _generateSkin();
    // _generateIds();
    // _generateSpine();
}

function _generateSheet() {
    console.log("资源打包成sheet")
    generateSheet(config.sheet_source, config.sheet_target);
}

// generateSkin(config.skin_source, config.skin_target);

function _generateSkin() {
    console.log("生成皮肤")
    generateSkin(config.skin_source, config.skin_target, config.egret_sheet,  _generateMC)
}

// 遍历界面,生成可视对象
function _generateIds() {
    console.log(config.skin_target,  config.skin_code);
    generateViewId(config.skin_target,  config.skin_code)
}

function _generateMC() {
    console.log("生成mc");
}

function _generateSpine() {
    generateSpine()
}