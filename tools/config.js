module.exports = {
    skin_source:"./egret_template/resource/eui_skins",
    skin_target:"./assets/skin",
    skin_code:'./src/skin',
    egret_sheet:'./egret_template/resource/assets/sheet',
    sheet_source:'./egret_template/resource/sheet',
    sheet_target:'./assets/sheet'
}