var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var BImage = (function (_super) {
    __extends(BImage, _super);
    function BImage() {
        var _this = _super.call(this) || this;
        _this.touchEnabled = true;
        _this.touchScaleEffect = true;
        return _this;
    }
    BImage.prototype.setPos = function (x, y) {
        this.x = x;
        this.y = y;
    };
    Object.defineProperty(BImage.prototype, "touchScaleEffect", {
        set: function (enabled) {
            if (enabled) {
                this.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.onTouchBegin, this);
            }
            else {
                this.removeEventListener(egret.TouchEvent.TOUCH_BEGIN, this.onTouchBegin, this);
            }
        },
        enumerable: true,
        configurable: true
    });
    BImage.prototype.onTouchBegin = function () {
        var view = this;
        var lastX = view.x;
        var lastY = view.y;
        var lastScaleX = view.scaleX;
        var lastScaleY = view.scaleY;
        var scale = 0.9;
        var tween = egret.Tween.get(view);
        var obj1 = { scaleX: lastScaleX * scale, scaleY: lastScaleY * scale, x: lastX + view.width * (1 - scale) / 2, y: lastY + view.height * (1 - scale) / 2 };
        var obj2 = { scaleX: lastScaleX, scaleY: lastScaleY, x: lastX, y: lastY };
        tween.to(obj1, 100, egret.Ease.sineIn).to(obj2, 100, egret.Ease.sineOut);
    };
    BImage.prototype.release = function () {
        this.removeEventListener(egret.TouchEvent.TOUCH_BEGIN, this.onTouchBegin, this);
        egret.Tween.removeTweens(this);
        this.setPos(0, 0);
    };
    return BImage;
}(eui.Image));
__reflect(BImage.prototype, "BImage");
window["BImage"] = BImage;
//# sourceMappingURL=BImage.js.map