import './manifest';
import Main from './src/main';

let app = createApp(640, 1068)
const main = new Main(app);
app.stage.addChild(main);

/**
 * 创建app
 * 
 * @param {*} width 
 * @param {*} height 
 * @param {*} scaleMode 
 * @param {*} backgroundColor 
 */
function createApp(width, height, scaleMode = "fixedWidth", backgroundColor = 0x25284A) {
    const windowWidth = window.innerWidth;
    const windowHeight = window.innerHeight;

    if (scaleMode == 'fixedWidth') {
        height = width * (windowHeight / windowWidth);
    } else if (scaleMode == 'fixedHeight') {
        width = height * (windowWidth / windowHeight);
    } 
    width = Math.ceil(width);
    height = Math.ceil(height);

    const scaleX = width / windowWidth;
    const scaleY = height / windowHeight;
    PIXI.interaction.InteractionManager.prototype.mapPositionToPoint = (point, x, y) => {
        point.x = x * scaleX;
        point.y = y * scaleY;
    }

    let options = {
        width, 
        height, 
        backgroundColor,
        view:canvas
    };
    
    console.log("baseurl:", PIXI.loader.baseUrl);
    console.log("width:", width);
    console.log("height:", height);
    let app = new PIXI.Application(options);
    return app;
}