
import Component from './Component';
class Image extends Component {
    constructor() {
        super();
        this._source = "";
    }

    get source() {
        return this._source;
    }

    set source(value) {
        if (this._source != value) {
            this._source = value;
        }
    }
}
