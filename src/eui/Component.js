const Container = PIXI.Container;
export default class Component extends Container {
    constructor() {
        super();
    }
    
    setTheme(theme) {
        if (theme === this.theme && theme) {
            return;
        }
        this.theme = theme;
        this.invalidSkin = true;
    }

    updateTransform() {
        if (!this.parent) {
            return;
        }
        if (this.redraw) {
            this.redraw();
        }
        super.updateTransform();
    }

    redraw() {
        // remove last skin after new one has been added
        // (just before rendering, otherwise we would see nothing for a frame)
        if (this._lastSkin) {
            //this.removeChild(this._lastSkin);
            this._lastSkin.alpha = 0;
            this._lastSkin = null;
        }
        if (this.invalidState) {
            this.fromSkin(this._currentState, this.changeSkin);
        }
        var width = this.worldWidth;
        var height = this.worldHeight;
        if (this._currentSkin && (this._lastWidth !== width || this._lastHeight !== height) && width > 0 && height > 0) {
            this._currentSkin.width = this._lastWidth = width;
            this._currentSkin.height = this._lastHeight = height;
            this.updateDimensions();
        }
    }

    updateDimensions() {

    }

    mousePos(e) {
        return e.data.getLocalPosition(this);
    };

    get enabled() {
        return this._enabled;
    }

    set enabled(value) {
        this._enabled = value;
    }

    get height() {
        return this._height
    }

    set height(value) {
        this._height = value
        this.minHeight = Math.min(value, this.minHeight)
    }

    get width() {
        return this._width
    }

    set width(value) {
        this._width = value
        this.minWidth = Math.min(value, this.minWidth)
    }
}
