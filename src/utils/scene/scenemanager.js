function SceneManager() {
    this.sceneMap = {};
    this.curScene = '';
}

Object.assign(SceneManager.prototype, {
    add:function(key, value) {
        this.sceneMap[key] = value;
    },

    remove:function(key) {
        let scene = this.sceneMap[key];
        if (scene) {
            scene.destory();
            delete this.sceneMap[key]
        }
    },

    enter:function(key) {
        if (key) {
            if (this.sceneMap[key]) {
                let value = this.sceneMap[key];
                let type = typeof value;
                if (type == 'function') {
                    let scene = new type();
                    this.sceneMap[key] = scene;
                }
            }
            this.curScene = key;
        } else {
            console.warn()
        }
    }
})

export default SceneManager