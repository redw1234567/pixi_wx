function Scene() {
    PIXI.Container.call(this);
    this.key = '';
    this.load = null;
    this.sound = null;
    this.stage = null;
    this.time = null;
    this.tween = null;
};

Scene.prototype = Object.create(PIXI.Container.prototype)

Object.assign(Scene.prototype, {
    init: function () {

    },

    create: function () {

    },

    update: function () {

    },

    shutdown: function () {

    },

    destory:function() {
        
    }
})
