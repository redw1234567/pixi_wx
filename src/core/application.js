import Preload from '../preload';
import MainView from '../main';

class Application{
    constructor(scaleMode = 'fixedWidth', baseValue = 640) {
        var app = this._createApp(scaleMode, baseValue);

        Application.stage = app.stage;
        Application.width = app.screen.width;
        Application.height = app.screen.height;
        Application.app = app;
        window.Application = Application;

        var preload = new Preload();
        preload.once("complete", this.onComplete, this)
        app.stage.addChild(preload)
    }

    _createApp(scaleMode, baseValue) {
        const pixelRatio = window.devicePixelRatio;
        const windowWidth = window.innerWidth;
        const windowHeight = window.innerHeight;
        const baseWidth = baseValue;
        let width = windowWidth * pixelRatio;
        let height = windowHeight * pixelRatio;
        if (scaleMode == 'fixedWidth') {
            width = baseWidth;
            height = baseWidth * (windowHeight / windowWidth);
        } else if (scaleMode == 'fixedHeight') {
            width = baseHeight * (windowWidth / windowHeight);
            height = baseHeight;
        } 
        width = Math.ceil(width);
        height = Math.ceil(height);
    
        let scaleX = width / windowWidth;
        let scaleY = height / windowHeight;
        PIXI.interaction.InteractionManager.prototype.mapPositionToPoint = (point, x, y) => {
            point.x = x * scaleX;
            point.y = y * scaleY;
        }
    
        let config = {width:width, height:height, backgroundColor:0x25284A};
        if (window.canvas) {
            config.view = window.canvas;
        }
        console.log("配置文件:", config)
        return new PIXI.Application(config);
    }

    onComplete() {
        let app = Application.app;
        app.stage.removeChild();
        var main = new MainView();
        app.stage.addChild(main);
        console.log(app.sound, '......')
        app.sound.playSound("assets/sound/action_god_missle.mp3")
        
    }

    static from(scaleMode, baseValue) {
        return new Application(scaleMode, baseValue);
    }
}

export default Application;