/**
 * 游戏入口
 * 
 * @param {number} width 
 * @param {number} height
 * @param {*} options 
 */
 var style = new PIXI.TextStyle({
    fontFamily: 'Arial',
    fontSize: 36,
    fontStyle: 'italic',
    fontWeight: 'bold',
    fill: ['#ffffff', '#00ff99'], // gradient
    stroke: '#4a1850',
    strokeThickness: 5,
    dropShadow: true,
    dropShadowColor: '#000000',
    dropShadowBlur: 4,
    dropShadowAngle: Math.PI / 6,
    dropShadowDistance: 6,
    wordWrap: true,
    wordWrapWidth: 440
});
class Main extends PIXI.Container {
    constructor(app) {
        super();
        app.loader.add("json", "assets/sheet/common.json").load((loader, res)=>{
            this.loadComplete(loader, res);
        })
    }

    // updateTransform() {
    //     console.log("----------")
    // }

    loadComplete(loader, res) {
        console.log("res:", res);

        var label = new PIXI.Text("hello world!!!", style);
        label.anchor.set(0.5);
        label.position.set(320, 80);
        this.addChild(label);

        let sprite = PIXI.Sprite.from("common_0.png");
        sprite.x = 200;
        sprite.y = 200;
        this.addChild(sprite)
    }
}

export default Main;
