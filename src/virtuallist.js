export default class VirtualList extends PIXI.Container {
    constructor() {
        this._itemRendererFactory = null;
        this._dataProvider = null;
    }

    set itemRendererFactory(value) {
        this._itemRendererFactory = value;
    }

    get dataProvider() {
        return this._dataProvider;
    }

    set dataProvider(value) {
        this._dataProvider = value;
    }
}



// let list = new PIXI.Gown.List({});
// let array = item.dataProvider;
// list.dataProvider = new PIXI.Gown.ListCollection(array);
// list.itemRendererFactory = createListItem;
// list.width = +item.width;
// list.height = +item.height;
// list.allowMultipleSelection = false;
// list.x = item.x || 0;
// list.y = item.y || 0;
