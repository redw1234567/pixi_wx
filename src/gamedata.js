function DataManager() {
    this.list = {};
    this.values = {};
    this._frozen = false;
    PIXI.utils.EventEmitter.call(this);
}
Object.assign(DataManager.prototype, PIXI.utils.EventEmitter.prototype);

Object.assign(DataManager.prototype, {
    get: function (key) {
        var list = this.list;
        if (Array.isArray(key)) {
            var output = [];
            for (var i = 0; i < key.length; i++) {
                output.push(list[key[i]]);
            }
            return output;
        } else {
            console.log(list)
            return list[key];
        }
    },
    set: function (key, data) {
        if (!this._frozen) {
            if (typeof key === 'string') {
                return this._set(key, data);
            } else {
                for (var entry in key) {
                    this._set(entry, key[entry]);
                }
            }
        }
        return this;
    },
    remove: function (key) {
        if (!this._frozen) {
            if (Array.isArray(key)) {
                for (var i = 0; i < key.length; i++) {
                    this._remove(key[i]);
                }
            } else {
                return this._remove(key);
            }
        }
        return this;
    },
    has: function (key) {
        return this.list.hasOwnProperty(key);
    },
    query: function (search) {
        var results = {};
        for (var key in this.list) {
            if (this.list.hasOwnProperty(key) && key.match(search)) {
                results[key] = this.list[key];
            }
        }
        return results;
    },
    getAll: function () {
        var results = {};
        for (var key in this.list) {
            if (this.list.hasOwnProperty(key)) {
                results[key] = this.list[key];
            }
        }
        return results;
    },
    _set: function (key, data) {
        if (!this._frozen) {
            if (this.has(key)) {
                this.values[key] = data;
            } else {
                var _this = this;
                var list = this.list;
                Object.defineProperty(this.values, key, {
                    enumerable: true,
                    configurable: true,
                    get: function () {
                        return list[key];
                    },
                    set: function (value) {
                        if (!_this._frozen) {
                            var previousValue = list[key];
                            if (previousValue !== value) {
                                list[key] = value;
                                _this.emit('changedata', key, value, previousValue);
                            }
                        }
                    }
                });
                list[key] = data;
                this.emit('setdata', key, data);
            }
        }
        return this;
    },
    _remove: function (key) {
        if (this.has(key)) {
            var data = this.list[key];
            delete this.list[key];
            delete this.values[key];
            let events = this;
            events.emit('removedata', key, data);
        }
        return this;
    },
    freeze: {
        get: function () {
            return this._frozen;
        },
        set: function (value) {
            this._frozen = (value) ? true : false;
        }
    },
    count: {
        get: function () {
            var i = 0;
            for (var key in this.list) {
                if (this.list[key] !== undefined) {
                    i++;
                }
            }
            return i;
        }
    },
    reset: function () {
        for (var key in this.list) {
            delete this.list[key];
            delete this.values[key];
        }
        this._frozen = false;
        return this;
    },
    destroy: function () {
        this.reset();
        this.off('changedata');
        this.off('removedata');
        this.parent = null;
    }
})

function getaa() {

}
export {getaa};
export default DataManager

