function setUserCloudStorage(params) {
    wx.setUserCloudStorage({
        KVDataList: params,
        success: function () {
            console.log("[wx.setUserCloudStorage] - success");
        },
        fail: function () {
            console.log("[wx.setUserCloudStorage] - fail");
        },
        complete: function () {
            console.log("[wx.setUserCloudStorage] - complete");
        }
    });
}

function getGroupCloudStorage(ticket) {
    let shareTicket = ticket
    let keyList = ["score", "uid"];
    wx.postMessageToOpenData({ key: "showGroupRank", ticket: shareTicket, keyList: keyList, ownUid: platform.uid });
}


function getFriendCloudStorage() {
    let keyList = ["score", "uid"];
    wx.postMessageToOpenData({ key: "showRank", keyList: keyList, ownUid: platform.uid });
}

function  getResultFriendCloudStorage() {
    let keyList = ["score", "uid"];
    wx.postMessageToOpenData({ key: "showFriendResult", keyList: keyList, ownUid: platform.uid });
}

function getResultNewCloudStorage() {
    let keyList = ["score", "uid"];
    wx.postMessageToOpenData({ key: "showNewResult", keyList: keyList, ownUid: platform.uid });
}

function getOverFriendCloudStorage(currentScore,isReset) {
    let keyList = ["score", "uid"];
    wx.postMessageToOpenData({ key: "showOverFriend", keyList: keyList, ownUid: platform.uid,currentScore:currentScore,isReset:isReset});
}

export {
    setUserCloudStorage,
    getOverFriendCloudStorage,
    getResultNewCloudStorage,
    getResultFriendCloudStorage,
    getFriendCloudStorage,
    getGroupCloudStorage
}
