function removeStorage(key){
    wx.removeStorageSync(key)
}

function setStorage(key, data) {
    wx.setStorageSync(key, data)
}

function getStorage(key) {
    return wx.getStorageSync(key)
}

export {
    removeStorage,
    setStorage,
    getStorage
}