const fileSystem = wx.getFileSystemManager();

/**
 * 读取本地文件内容
 * 
 * object.filePath 要读取的文件的路径
 * object.encoding=binary 合法的encoding值 ascii base64 binary	hex	ucs2/ucs-2/utf16le/utf-16le	以小端序读取 utf-8/utf8	latin1
 * object.success=null 接口调用成功的回调函数	
 * object.fail=null 接口调用失败的回调函数
 * object.complete=null 接口调用结束的回调函数（调用成功、失败都会执行）
 */
export function readFile(object) {
    if (!object || !object.filePath) {
        console.error('读取文件参数错误');
    } else {
        fileSystem.readFile(object);
    }
}

/**
 * 同步读取本地文件内容
 * 
 * filePath 路径
 * encoding 编码 合法的encoding值 ascii base64 binary	hex	ucs2/ucs-2/utf16le/utf-16le	以小端序读取 utf-8/utf8	latin1
 */
export function readFileSync(filePath, encoding = 'binary') {
    return fileSystem.readFileSync(filePath, encoding)
}

/**
 * 保存临时文件到本地。此接口会移动临时文件，因此调用成功后，tempFilePath 将不可用。
 * 
 * tempFilePath	string		是	临时存储文件路径	
 * filePath	string		是	要存储的文件路径	
 * success	function		否	接口调用成功的回调函数	
 * fail	function		否	接口调用失败的回调函数	
 * complete	function		否	接口调用结束的回调函数（调用成功、失败都会执行）
 */
export function saveFile(object) {
    if (!object || !object.tempFilePath) {
        console.error('保存文件参数错误');
    } else {
        fileSystem.saveFile(object);
    }
}

/**
 * 同步保存临时文件到本地
 * 
 * tempFilePath 临时存储文件路径
 * filePath 要存储的文件路径
 * 返回 存储后的文件路径
 */
export function svaeFileSync(tempFilePath, filePath) {
    if (!tempFilePath || !filePath) {
        console.error('同步保存文件参数错误');
    } else {
        fileSystem.svaeFileSync(tempFilePath, filePath);
    }
}