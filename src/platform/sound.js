let curMusic = null;
const soundList = [];

// 播放声音
export function playSound(src){
    let sound = null;
    for (let i = 0, len = soundList.length; i < len; i++) {
        if (soundList[i].paused) {
            sound = soundList[i];
            break;
        }
    }
    let self = this;
    if (!sound) {
        sound = wx.createInnerAudioContext();
        soundList.push(sound);
    }
    sound.src = src;
    sound.play();
}

// 停止声音
export function stopSound(sound){
	console.log(sound);
}

	// 播放音乐
export function playMusic(src){
    if (!curMusic) {
        curMusic = wx.createInnerAudioContext();
    }
    if (curMusic) {
        if (curMusic.src != src) {
            curMusic.src = src;
        }
    }
    if (curMusic.paused) {
        curMusic.play();
    }
}

// 停止音乐
export function stopMusic(){
    if (curMusic) {
        curMusic.stop();
    }
}