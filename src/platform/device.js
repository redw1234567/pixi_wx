/**
 * 开始监听加速度数据。
 * 
 * @param {*} object 
 */
function startAccelerometer(object) {
    wx.startAccelerometer(object);
}

/**
 * 停止监听加速度数据。
 * 
 * @param {*} object 
 */
function stopAccelerometer(object) {
    wx.stopAccelerometer(object);
}

/**
 * 监听加速度数据事件
 */
function onAccelerometerChange(callback) {
    wx.onAccelerometerChange(callback)
}

/**
 * 监听罗盘数据变化事件。
 * 频率：5 次/秒，接口调用后会自动开始监听，可使用 wx.stopCompass 停止监听。
 */
function onCompassChange(callback) {
    wx.onCompassChange(callback)
}

/**
 * 开始监听罗盘数据
 */
function startCompass(object) {
    wx.startCompass(object);
}

/**
 * 停止监听罗盘数据
 */
function stopCompass(object) {
    wx.stopCompass(object);
}

/**
 * 监听陀螺仪数据变化事件
 */
function onGyroscopeChange(callback) {
    wx.onGyroscopeChange(callback)
}

/**
 * 开始监听陀螺仪数据。
 */
function startGyroscope(object) {
    wx.startGyroscope(object);
}

/**
 * 停止监听陀螺仪数据。
 */
function stopGyroscope() {
    wx.stopGyroscope(object);
}

/**
 * 获取设备电量 level	string	设备电量，范围 1 - 100 isCharging	boolean	是否正在充电中
 */
function getBatteryInfoSync() {
    return wx.getBatteryInfoSync();
}

/**
 * 设置系统剪贴板的内容
 */
function setClipboardData(object) {
    wx.setClipboardData(object)
}

/**
 * 获取系统剪贴板的内容
 */
function getClipboardData(object) {
    wx.getClipboardData(object)
}

/**
 * 获取网络类型
 */
function getNetworkType(object) {
    wx.getNetworkType(object)
}

/**
 * 监听网络状态变化事件
 * 
 * @param {*} callback
 */
function onNetworkStatusChange(callback) {
    wx.onNetworkStatusChange(callback)
}

/**
 * 使手机发生较短时间的振动（15 ms）。仅在 iPhone 7 / 7 Plus 以上及 Android 机型生效
 * 
 * @param {*} object 
 */
function vibrateShort(object) {
    wx.vibrateShort(object)
}

/**
 * 使手机发生较长时间的振动（400 ms)
 */
function vibrateLong() {
    wx.vibrateLong(object)
}

export {
    startAccelerometer,
    stopAccelerometer,
    onAccelerometerChange,
    startCompass,
    stopCompass,
    onCompassChange,
    startGyroscope,
    stopGyroscope,
    onGyroscopeChange,
    getBatteryInfoSync,
    setClipboardData,
    getClipboardData,
    getNetworkType,
    onNetworkStatusChange,
    vibrateLong,
    vibrateShort
}

