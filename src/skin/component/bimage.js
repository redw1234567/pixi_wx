export default class BImage extends PIXI.Sprite {
    constructor(texture) {
        super(texture);
        this.touchEffect = true;
        this.interactive = true;
    }

    set touchEffect(value) {
        this.off('pointerdown', this.onTouchBegin, this);
        if (value) {
            this.on('pointerdown', this.onTouchBegin, this);
        }
            
    }

    onTouchBegin() {
        let view = this;
        let scale = 0.9;
        let lastScaleX = view.scale.x || 1;
        let lastScaleY = view.scale.y || 1;
        let tween = Application.tweenManager.createTween(view.scale);
        let obj = {x:lastScaleX * scale, y: lastScaleY * scale};
        tween.time = 100;
        tween.pingPong = true;
        tween.to(obj);
        tween.easing = PIXI.Easing.inOutSine();
        tween.start();
    }

    destory() {
        super.destroy();
        this.off('pointerdown', this.onTouchBegin, this);
    }

    static from(key, scale9) {
        console.log(key, "--------------------------------------")
        let sprite;
        if (scale9) {
            let arr = scale9.split(",");
            let left = +arr[0];
            let top = +arr[1];
            let right = +arr[2];
            let bottom = +arr[3];
            let texture = PIXI.Texture.from(key);
            right = texture.orig.width - left - right;
            bottom = texture.orig.height - top - bottom;
            sprite = new PIXI.NineSlicePlane(PIXI.Texture.from(key), left, top, right, bottom);
        } else {
            sprite = new BImage(PIXI.Texture.from(key));
        }
        return sprite;
    }
}
